import {Component, OnInit} from '@angular/core';
import { LoggingService } from '../../services/logging.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  homeTitle = 'Welcome to homepage!';

  constructor(private logging: LoggingService) { }

  ngOnInit() {
  }
}
