import {Component, OnInit} from '@angular/core';
import {AppFilterPipe} from '../../pipes/app-filter.pipe';
import {DataService} from '../../services/data.service';
declare const firebase: any;

@Component({
  selector: 'app-directory',
  templateUrl: './directory.component.html',
  styleUrls: ['./directory.component.css']
})
export class DirectoryComponent implements OnInit {
  ninjas = [];

  term: string;

  constructor(private dataServive: DataService) {}

  ngOnInit() {
    // this.dataServive.fetchData().subscribe(
    //   (data) => this.ninjas = data
    // );
    this.fbGetData();
  }

  fbGetData() {
    firebase.database().ref('/').on('child_added', (snapshot) => {
      this.ninjas.push(snapshot.val());
    });
  }

  fbPostData(name, belt) {
    firebase.database().ref('/').push({name: name, belt: belt});
  }
}
