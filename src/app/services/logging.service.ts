import { Injectable } from '@angular/core';

@Injectable()
export class LoggingService {
  data: string[] = [];

  constructor() {}

  log() {
    console.log(this.data);
  }
}
