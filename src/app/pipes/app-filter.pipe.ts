import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'app_filter'
})
export class AppFilterPipe implements PipeTransform {
  transform(namesArray: any, term: any): any {
    // check if search term is undefined
    if (term === undefined) {
      return namesArray;
    }
    // return updated ninjas array
    return namesArray.filter((item) => {
      return item.name.toLowerCase().includes(term.toLowerCase());
    });
  }
}
